using Mirror;
using UnityEngine;
using TMPro;

public class GameManager : NetworkBehaviour {
    private static GameManager Instance;
    
    public Ball prfBall;
    public float speedPlayer = 50;
    public float speedBall = 20;
    public ParticleSystem vfxSparks;

    public TextMeshProUGUI txtScore { get; set; }

    // TODO: SyncVar
    [SyncVar(hook = nameof(OnChangedScore))]
    private int score;

    // TODO: NetworkBehaviour override
    public override void OnStartClient() {
        Instance = this;

        txtScore = FindObjectOfType<TextMeshProUGUI>();
        txtScore.text = "SCORE: 0";
    }

    public static GameManager GetInstance() {
        if (!Instance) {
            Instance = FindObjectOfType<GameManager>();
        }
        
        return Instance;
    }
    
    public void IncrementScore() {
        score += 100;
    }

    // TODO: SyncVar hook method
    private void OnChangedScore(int _old, int _new) {
        txtScore.text = "SCORE: " + score;
    }
}
