using Mirror;

/// <summary>
/// Responsible for spawning all NetworkIdentities 
/// already existing in the scene for connected clients.
/// </summary>
public class SceneSetup : NetworkBehaviour {
    // NetworkBehaviour override
    public override void OnStartServer() {
        // TODO: Server-only - Destroy for client
        if (!isServer) {
            Destroy(gameObject);
        }

        NetworkServer.SpawnObjects();
    }
}
