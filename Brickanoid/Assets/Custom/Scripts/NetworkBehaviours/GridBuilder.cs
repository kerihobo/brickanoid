using Mirror;
using UnityEngine;

/// <summary>
/// Distributes tiles in rows within the play-space.
/// Tiles have set colours per row but have randomized tonal differences and widths.
/// </summary>
public class GridBuilder : NetworkBehaviour {
    public static GridBuilder Instance;

    public Color[] rowColors = new Color[5];
    
    public Vector2 minMaxColorVariation { get; set; }
    
    [SerializeField]
    private Vector2 minMaxBrickWidth = new Vector2(.5f, 1.3f);
    [SerializeField]
    private Tile prfTile;
    [SerializeField]
    private float gridWidth = 6;
    [SerializeField]
    private float colorVariation = .4f;

    // TODO: NetworkBehaviour override
    public override void OnStartServer() {
        Instance = this;

        Tile.count = 0;

        minMaxColorVariation = new Vector2(1 - (colorVariation / 2), 1 + (colorVariation / 2));

        Spawn();
    }

    public void Spawn() {
        for (int i = 0; i < rowColors.Length; i++) {
            SpawnRow(i);
        }
    }

    /// <summary>
    /// Keep spawning tiles at different widths until the row is full.
    /// </summary>
    /// <param name="i"></param>
    private void SpawnRow(int i) {
        float remainingLength = gridWidth;
        float xStart = -(gridWidth / 2);

        while (remainingLength > 0) {
            SpawnTile(i, ref remainingLength, xStart);
        }
    }

    /// <summary>
    /// Spawn a tile at a random width within the bounds of remaining length.
    /// Each tile is coloured for the row it belongs to but with a slight tonal difference.
    /// </summary>
    /// <param name="_i"></param>
    /// <param name="_remainingLength"></param>
    /// <param name="_xStart"></param>
    private void SpawnTile(int _i, ref float _remainingLength, float _xStart) {
        float width = GetNewWidth(ref _remainingLength);
        Vector2 newPosition = GetNewStartPosition(_i, _remainingLength, _xStart, width);

        Tile newTile = Instantiate(prfTile, newPosition, Quaternion.identity);
        // TODO: Spawn
        NetworkServer.Spawn(newTile.gameObject);

        newTile.transform.localScale = new Vector2(width, 1);
        newTile.SetColor(rowColors[_i]);

        Tile.count++;

        Vector2 GetNewStartPosition(int _i, float _remainingLength, float _xStart, float _width) {
            float y = transform.position.y - _i;
            float x = _xStart - (_width / 2) + (gridWidth - _remainingLength);
        
            return new Vector2(x, y);
        }

        float GetNewWidth(ref float _remainingLength) {
            float width = UnityEngine.Random.Range(minMaxBrickWidth.x, minMaxBrickWidth.y);

            _remainingLength -= width;
            if (_remainingLength < 0) {
                width += _remainingLength;
                _remainingLength = 0;
            }

            return width;
        }
    }
}
