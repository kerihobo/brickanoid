using Mirror;
using System.Collections;
using UnityEngine;

public class Tile : NetworkBehaviour {
    public static int count;
    
    private SpriteRenderer spr;
    // TODO: SyncVar
    [SyncVar(hook = nameof(OnColorChanged))]
    private Color color;

    private void Awake() {
        spr = GetComponent<SpriteRenderer>();
    }

    /// <summary>
    /// Take the row-colour and randomize the tone. Purely for looks.
    /// </summary>
    /// <param name="_color"></param>
    public void SetColor(Color _color) {
        Vector2 minMax = GridBuilder.Instance.minMaxColorVariation;
        Color newColor = _color * Random.Range(minMax.x, minMax.y);
        newColor.a = 1;

        color = newColor;
    }

    // TODO: SyncVar hook method
    private void OnColorChanged(Color _old, Color _new) {
        spr.color = _new;
    }

    /// <summary>
    /// Add score. Get every client to run the tweened destruction sequence.
    /// </summary>
    /// <param name="_direction"></param>
    public void Destroy(Vector2 _direction) {
        GameManager.GetInstance().IncrementScore();

        AnimateDestruction(_direction);
    }

    // TODO: ClientRPC
    [ClientRpc]
    private void AnimateDestruction(Vector2 _direction) {
        GetComponent<Collider2D>().enabled = false;
        StartCoroutine("DestroySequence", _direction);
    }

    /// <summary>
    /// When hit by the ball, Tween in the hit-direction
    /// then destroy. If it was the last tile, rebuild the grid.
    /// </summary>
    /// <param name="_direction"></param>
    /// <returns></returns>
    public IEnumerator DestroySequence(Vector2 _direction) {
        Vector2 vStart = transform.position;
        Vector2 vEnd = vStart + (_direction.normalized / 2);
        Color clStart = spr.color;
        Color clEnd = spr.color;
        clEnd.a = 0;

        float t = 0;
        float duration = .25f;

        while (t < duration) {
            transform.position = Vector2.Lerp(vStart, vEnd, t / duration);
            spr.color = Color.Lerp(clStart, clEnd, t / duration);

            t += Time.deltaTime;
            yield return null;
        }

        // TODO: Server-only
        if (isServer) {
            count--;
            
            if (count == 0) {
                GridBuilder.Instance.Spawn();
            }

            // TODO: Destroy
            NetworkServer.Destroy(gameObject);
        }
    }
}
