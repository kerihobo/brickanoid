using Mirror;
using UnityEngine;

public class Ball : NetworkBehaviour {
    // TODO: SyncVar
    [SyncVar(hook = nameof(OnChangedPlayer))]
    private Player player;
    
    public Rigidbody2D rb { get; set; }

    private Vector2 lastVelocity;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (rb.bodyType == RigidbodyType2D.Kinematic) {
            transform.position = player.spawnPointBall.position;
        }

        // TODO: Server-only - Destroy
        if (isServer && !player) {
            NetworkServer.Destroy(gameObject);
        }
    }

    private void LateUpdate() {
        if (isServer) {
            lastVelocity = rb.velocity;
        }
    }

    private void FixedUpdate() {
        // TODO: Server-only
        if (!isServer) return;

        if (rb.velocity.magnitude > 0.2f) {
            rb.velocity = rb.velocity.normalized * GameManager.GetInstance().speedBall;
        }

        EscapePureHorizontalVelocity();

        void EscapePureHorizontalVelocity() {
            if (Mathf.Abs(Vector3.Dot(rb.velocity.normalized, Vector3.left)) <= .95f) {
                return;
            }
            if (Camera.main.WorldToScreenPoint(transform.position).y > Screen.height / 2) {
                rb.AddForce(Vector2.down * 20);
            } else {
                rb.AddForce(Vector2.up * 20);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D _other) {
        // TODO: Server-only
        if (!isServer) return;
        
        if (_other.CompareTag("Killzone")) {
            ReturnBall();
        }
    }

    public void Initialize(Player _player) {
        player = _player;
    }

    // TODO: ClientRPC
    [ClientRpc]
    private void ReturnBall() {
        rb.bodyType = RigidbodyType2D.Kinematic;
        rb.velocity = Vector2.zero;
        transform.position = player.spawnPointBall.position;
    }

    private void OnCollisionEnter2D(Collision2D _other) {
        GameManager.GetInstance().vfxSparks.transform.position = _other.contacts[0].point;
        GameManager.GetInstance().vfxSparks.Emit(30);

        if (_other.gameObject.CompareTag("Tile")) {
            // TODO: Server-only
            if (isServer) {
                _other.gameObject.GetComponent<Tile>().Destroy(lastVelocity);
            }
        }
    }

    // TODO: SyncVar hook method
    private void OnChangedPlayer(Player _old, Player _new) {
        transform.position = player.spawnPointBall.position;
        GetComponent<SpriteRenderer>().color = player.color;

        TrailRenderer tr = GetComponent<TrailRenderer>();
        tr.startColor = player.color;
        tr.endColor = player.color;
    }

    // TODO: ClientRPC
    [ClientRpc]
    public void Launch(Vector2 _direction) {
        transform.position = player.spawnPointBall.position;
        rb.bodyType = RigidbodyType2D.Dynamic;

        // TODO: Server-only
        if (isServer) {
            rb.velocity = _direction * GameManager.GetInstance().speedBall;
        }
    }

    /// <summary>
    /// If the ball is dynamic, it means it hasn't been launched yet.
    /// </summary>
    /// <returns></returns>
    public bool IsNewBall => rb.bodyType != RigidbodyType2D.Dynamic;
}
