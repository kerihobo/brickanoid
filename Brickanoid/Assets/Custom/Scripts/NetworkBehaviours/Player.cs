using Mirror;
using UnityEngine;

public class Player : NetworkBehaviour {
    public Transform spawnPointBall;

    [HideInInspector, SyncVar(hook = nameof(OnColorChanged))]
    public Color color;

    private static Player[] activePlayers = new Player[5];

    private Rigidbody2D rb;
    private SpriteRenderer spr;
    private Vector2 initialPosition;
    private Vector2 mouseWorldPosition;
    // TODO: SyncVar
    [SyncVar]
    private Ball ball;
    // TODO: SyncVar
    private int id;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();

        initialPosition = rb.position;
    }

    // NetworkBehaviour override
    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();

        InitializeOnServer();
    }

    // TODO: Command
    [Command]
    public void InitializeOnServer() {
        SetColorByID();

        Ball newBall = Instantiate(GameManager.GetInstance().prfBall, spawnPointBall.position, Quaternion.identity);
        // TODO: Spawn
        NetworkServer.Spawn(newBall.gameObject);

        ball = newBall;
        ball.Initialize(this);
        
        // 5 colours are supported.
        // Pick the one that matches which activePlayers slot you occupy.
        void SetColorByID() {
            for (int i = 0; i < activePlayers.Length; i++) {
                if (activePlayers[i] == null) {
                    activePlayers[i] = this;
                    id = i;

                    break;
                }
            }

            color = GridBuilder.Instance.rowColors[id];
        }
    }

    void Update() {
        // TODO: Local player only
        if (!isLocalPlayer) return;
        
        SetMouseWorldPosition();

        bool isNewBall = ball && ball.IsNewBall;
        if (Input.GetMouseButtonDown(0) && isNewBall) {
            LaunchInRandomDirection();
        }
        
        void SetMouseWorldPosition() {
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mouseWorldPosition.y = initialPosition.y;
        }
        
        // TODO: Command
        [Command]
        void LaunchInRandomDirection() {
            float angle = Random.Range(-45f, 45f);
            spawnPointBall.localEulerAngles = Vector3.forward * angle;
            Vector2 direction = spawnPointBall.transform.up;

            ball.Launch(direction);
        }
    }

    private void FixedUpdate() {
        Move();
    }

    private void Move() {
        float maxDistanceDelta = Time.fixedDeltaTime * GameManager.GetInstance().speedPlayer;
        rb.MovePosition(Vector2.MoveTowards(rb.position, mouseWorldPosition, maxDistanceDelta));
    }

    // TODO: SyncVar hook method
    private void OnColorChanged(Color _old, Color _new) {
        spr.color = _new;
    }

    // Remove from activePlayers array.
    private void OnDestroy() {
        // TODO: Server-only
        if (isServer) {
            for (int i = 0; i < activePlayers.Length; i++) {
                if (activePlayers[i] == this) {
                    activePlayers[i] = null;

                    break;
                }
            }
        }
    }
}
