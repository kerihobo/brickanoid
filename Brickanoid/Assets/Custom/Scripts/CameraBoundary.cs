using UnityEngine;

/// <summary>
/// Sets up collision at the edges of the screen for the ball to bounce from.
/// </summary>
public class CameraBoundary : MonoBehaviour {
    private void Awake() {
        AddColliderToScreenBorders();
    }

    private void AddColliderToScreenBorders() {
        EdgeCollider2D edgeCollider = GetComponent<EdgeCollider2D>();

        Vector2 bottomLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        Vector2 topLeft = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height));
        Vector2 topRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        Vector2 bottomRight = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0));

        edgeCollider.points = new Vector2[] {
            bottomLeft
        ,   topLeft
        ,   topRight
        ,   bottomRight
        };
    }
}
